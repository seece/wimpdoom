# wimpdoom

A Doom mod where you can shoot only with your eyes closed.

![Winking David](http://i.imgur.com/6LVii9F.png)

## How does it work
To be honest, *not that well!*

An eyedetector program `eyedetect.exe` finds two eyes in your webcam image. When two eyes are found, it proceeds to check if the eyes are open or not. If the left or right eye is open, the `b` and `n` keys are held down respectively. In other words, `eyedetect.exe` send virtual keyboard events signaling the state of the eyes found in webcam picture.

A custom doom WAD then detects these events and allows you to shoot only when `b` and `n` keys are held down.

## Guide
### Requirements
 * **A webcam** (with good lighting, preferably straight at yer face)
 * **[ZDoom][zdoom]** to run the game
 * **Windows operating system** for the eye detector

### Usage
Bind the eye signaling keys in ZDoom by entering these commands to the console: `bind b +user1` and `bind n +user2`

Run `eyedetect.exe` that opens up a window where you can see your face and some visual markers showing where it thinks you eyes are. Now keys are getting pressed if you blink your eyes.

Then start ZDoom with the bundled `wimp.pk3` as a PWAD. This can be done with

	zdoom -iwad doom2.wad -file wimp.pk3

Now close both your eyes before attempting to shoot with a pistol.

### Caveats
* In the game only pistol shooting is restricted. See `mod/wimp/DECORATE.txt` for details.
* The eye detection is very fiddly and not reliable, which kinda ruins the fun. Sorry!

## The custom eye cascades
The custom cascades `lax_cascade.xml`, `less_strict_cascade.xml` and `strict_cascade.xml` are trained using the same dataset but different settings. They should detect a 24x24 open eye in an image. In this project they are used to detect if eyes are open or closed inside the capture eye-pair area.

They are all pretty bad but here for completeness. The default cascade used in the detector algorithm is `haarcascade_eye.xml`, which is bundled in a regular OpenCV install.

See *Training* section below for details.

### Convert an incomplete Haar cascade to usable form
This is just a technical tip.

`convert_cascade` applies only to older models trained with `haartraining`.
If the incomplete model (the directory with `params.xml`, `stage0.xml`, `stage1.xml` and so on) was trained with `opencv_traincascade`, you just need to run the training command again but with fewer stages. [Source](http://answers.opencv.org/question/7173/convert-opencv_traincascade-intermediate-output/?answer=7213#post-id-7213)

### Training
Training was done with some free eye images and with the command

	./traincascade -vec ~/training/eyes.vec -bg ~/training/negative.txt -w 24 -h 24 -data ~/training/strict -numStages 8 -numPos 2100 -numNeg 3576 -mem 1024 -mode ALL -minhitrate 0.999 -maxfalsealarm 0.5

Variations were made to `-minhitrate` (0.999 in `strict`, 0.995 in `lax`).


## Compiling

Get the source code at https://bitbucket.org/seece/wimpdoom/

### License
MIT license.

### C++
You need [Visual Studio 2013][vs2013]. The solution file expects to find an OpenCV 2.49 installation in a directory one level up from the solution dir.

### ACS
To compile the ACS scripts, put [ACC 1.54](http://zdoom.org/Download#acc) in `mod/acc` and run 

	acc\acc.exe -n wimp\scripts\wimp.acs wimp\acs\wimp.o


[zdoom]: http://zdoom.org/News
[vs2013]: http://www.visualstudio.com/products/visual-studio-community-vs
