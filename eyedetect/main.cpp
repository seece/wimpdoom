/**
* eye-tracking.cpp:
* Eye detection and tracking with OpenCV
*
* This program tries to detect and tracking the user's eye with webcam.
* At startup, the program performs face detection followed by eye detection
* using OpenCV's built-in Haar cascade classifier. If the user's eye detected
* successfully, an eye template is extracted. This template will be used in
* the subsequent template matching for tracking the eye.
* https://github.com/bsdnoobz/opencv-code/blob/master/eye-tracking.cpp
*/
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include "keys.h"

#include <iostream>

cv::CascadeClassifier face_cascade;
cv::CascadeClassifier eye_cascade;
cv::CascadeClassifier single_eye_cascade;

/**
* Function to detect human face and the eyes from an image.
*
* @param  im    The source image
* @param  tpl   Will be filled with the eye template, if detection success.
* @param  rect  Will be filled with the bounding box of the eye
* @return zero=failed, nonzero=success
*/
int detectEye(cv::Mat& im, cv::Mat& tpl, cv::Rect& rect, cv::Rect& out_face)
{
	std::vector<cv::Rect> faces, eyes;
	face_cascade.detectMultiScale(im, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, cv::Size(25, 25));

	for (int i = 0; i < faces.size(); i++)
	{
		cv::Mat face = im(faces[i]);
		eye_cascade.detectMultiScale(face, eyes, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, cv::Size(22, 5));

		if (eyes.size())
		{
			rect = eyes[0] + cv::Point(faces[i].x, faces[i].y);

			rect.x = cv::max(0, rect.x - 2);
			rect.width += 2;
			rect.y = cv::max(0, rect.y - 4);
			rect.height += 4;

			tpl = im(rect);

			//out_face = faces[i];
		}
	}

	return eyes.size();
}

bool checkIfEyeOpen(cv::Mat& eye_part, cv::Rect& out_rect)
{
	int w = eye_part.size().width;
	int h = eye_part.size().height;

	cv::Mat eye;

	if (h < 30) {
		int newh = 30;
		cv::resize(eye_part, eye, cv::Size(w*(newh / (float)h), newh));
	} else {
		eye = eye_part;
	}

	std::vector<cv::Rect> eyes;
	single_eye_cascade.detectMultiScale(eye, eyes, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, cv::Size(20, 20));

	for (int i = 0; i < eyes.size(); i++) {
		out_rect = eyes[i];
		return true;
	}

	return false;
}

/**
* Perform template matching to search the user's eye in the given image.
*
* @param   im    The source image
* @param   tpl   The eye template
* @param   rect  The eye bounding box, will be updated with the new location of the eye
*/
void trackEye(cv::Mat& im, cv::Mat& tpl, cv::Rect& rect)
{
	cv::Size size(rect.width * 2, rect.height * 2);
	cv::Rect window(rect + size - cv::Point(size.width / 2, size.height / 2));

	window &= cv::Rect(0, 0, im.cols, im.rows);

	//cv::Mat dst(window.width - tpl.rows + 1, window.height - tpl.cols + 1, CV_32FC1);
	cv::Mat dst(window.width - tpl.cols+ 1, window.height - tpl.rows + 1, CV_32FC1);
	cv::matchTemplate(im(window), tpl, dst, CV_TM_SQDIFF_NORMED);

	double minval, maxval;
	cv::Point minloc, maxloc;
	cv::minMaxLoc(dst, &minval, &maxval, &minloc, &maxloc);

	if (minval <= 0.2)
	{
		rect.x = window.x + minloc.x;
		rect.y = window.y + minloc.y;
	}
	else
		rect.x = rect.y = rect.width = rect.height = 0;
}

#define VERSION_STRING "v0.1"

int main(int argc, char** argv)
{
	std::cout << "wimpdoom eyedetector " << VERSION_STRING << std::endl;
	float eye_move_limit = 20.0f;

	std::string face_cascade_path = "haarcascade_frontalface_alt2.xml";
	std::string eye_cascade_path = "haarcascade_mcs_eyepair_small.xml";
	std::string single_eye_cascade_path = "haarcascade_eye.xml";

	if (argc > 2) face_cascade_path = std::string(argv[1]);
	if (argc > 3) eye_cascade_path = std::string(argv[2]);
	if (argc > 4) single_eye_cascade_path = std::string(argv[3]);
	if (argc > 5) eye_move_limit = std::stod(argv[4]);

	std::cout << "face_cascade_path: " << face_cascade_path << std::endl;
	std::cout << "eye_cascade_path: " << eye_cascade_path << std::endl;
	std::cout << "single_eye_cascade_path: " << single_eye_cascade_path << std::endl;
	std::cout << "Using eye_move_limit of " << eye_move_limit << std::endl;

	// Load the cascade classifiers
	// Make sure you point the XML files to the right path, or 
	// just copy the files from [OPENCV_DIR]/data/haarcascades directory
	face_cascade.load(face_cascade_path); //20x20
	//face_cascade.load("haarcascade_frontalface_default.xml"); //24x24
	//eye_cascade.load("haarcascade_eye.xml"); // 20x20?
	eye_cascade.load(eye_cascade_path); // 22x5
	//eye_cascade.load("haarcascade_mcs_eyepair_big.xml"); // 45x11
	single_eye_cascade.load(single_eye_cascade_path); // 20x20
	//single_eye_cascade.load("haarcascade_single_eye1.xml"); // 24x24
	//single_eye_cascade.load("haarcascade_single_eye1_lax.xml"); // 24x24
	//single_eye_cascade.load("haarcascade_single_eye1_strict.xml"); // 24x24
	//single_eye_cascade.load("haarcascade_single_eye1_less_strict.xml"); // 24x24

	// Open webcam
	cv::VideoCapture cap(0);

	// Check if everything is ok
	if (face_cascade.empty() || eye_cascade.empty() || !cap.isOpened())
		return 1;

	// Set video to 320x240
	cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);

	cv::Mat frame, eye_tpl, eye_tpl_rgb;
	cv::Mat left_eye_part, right_eye_part;
	cv::Mat screen(240, 320 * 2, CV_8UC3);
	cv::Rect eye_bb, old_eye_bb, face_bb;

	bool old_left_open = false;
	bool old_right_open = false;
	bool toggle_equalize = false;

	while (cv::waitKey(15) != 'q')
	{
		old_eye_bb = eye_bb;

		cap >> frame;
		if (frame.empty())
			break;

		// Flip the frame horizontally, Windows users might need this
		cv::flip(frame, frame, 1);

		// Convert to grayscale and 
		// adjust the image contrast using histogram equalization
		cv::Mat gray;
		/*cv::resize(frame, gray, cv::Size(320 * 0.75, 240 * 0.75));
		cv::cvtColor(gray, gray, CV_BGR2GRAY);*/
		cv::cvtColor(frame, gray, CV_BGR2GRAY);

		if (toggle_equalize)
			cv::equalizeHist(gray, gray);

		cv::Mat gray_bgr;
		cv::cvtColor(gray, gray_bgr, CV_GRAY2BGR);
		frame.copyTo(screen.rowRange(0, frame.size().height).colRange(320, 320 + frame.size().width));
		gray_bgr.copyTo(screen.rowRange(0, gray_bgr.size().height).colRange(0, gray_bgr.size().width));

		if (eye_bb.width == 0 && eye_bb.height == 0)
		{
			// Detection stage
			// Try to detect the face and the eye of the user
			detectEye(gray, eye_tpl, eye_bb, face_bb);

			cv::Size half(eye_tpl.size().width / 2, eye_tpl.size().height);
			left_eye_part = cv::Mat(half.height, half.width, CV_32FC1);
			right_eye_part = cv::Mat(half.height, half.width, CV_32FC1);
		}
		else
		{
			// Tracking stage with template matching
			trackEye(gray, eye_tpl, eye_bb);

			cv::Vec2f eye_move(old_eye_bb.x - eye_bb.x, old_eye_bb.y - eye_bb.y);

			if (cv::norm(eye_move) > eye_move_limit) {
				eye_bb.width = 0;
				eye_bb.height = 0;
				std::cout << "Too fast movement, resetting eye bounding box" << std::endl;
			}

			if (eye_bb.area() > 0) {
				auto size = left_eye_part.size();
				int w = size.width;
				int h = size.height;

				left_eye_part = gray.rowRange(eye_bb.y, eye_bb.y + size.height).colRange(eye_bb.x, eye_bb.x + size.width);
				right_eye_part = gray.rowRange(eye_bb.y, eye_bb.y + size.height).colRange(eye_bb.x + size.width, eye_bb.x + 2*size.width);

				//cv::equalizeHist(left_eye_part, left_eye_part);
				//cv::equalizeHist(right_eye_part, right_eye_part);

				bool left_open = false;
				bool right_open = false;
				cv::Rect left_eye_rect, right_eye_rect;

				left_open = checkIfEyeOpen(left_eye_part, left_eye_rect);
				right_open = checkIfEyeOpen(right_eye_part, right_eye_rect);

				if (left_open && !old_left_open) {
					pressKey(0x30, false);
				} 

				if (!left_open && old_left_open) {
					// 0x42 = b virtual keym, 0x62 = b scancode
					pressKey(0x30, true); 
				}

				if (right_open && !old_right_open) {
					pressKey(0x31, false);
				} 

				if (!right_open && old_right_open) {
					pressKey(0x31, true); 
				}

				old_left_open = left_open;
				old_right_open = right_open;

				const char* left_text = "L closed";
				const char* right_text = "R closed";
				cv::Point left_textpos(eye_bb.x, eye_bb.y - 25);
				cv::Point right_textpos(eye_bb.x + 50, eye_bb.y - 25);

				if (left_open) {
					left_text = "L open";
					left_textpos = cv::Point(eye_bb.x, eye_bb.y - 25);
					cv::line(screen, cv::Point(eye_bb.x + left_eye_rect.width/2, eye_bb.y + left_eye_rect.height/2), left_textpos, cvScalar(0, 0, 255));
					cv::rectangle(screen, cv::Rect(eye_bb.x + left_eye_rect.x, eye_bb.y + left_eye_rect.y, left_eye_rect.width, left_eye_rect.height), cv::Scalar(255, 0, 0));
				}

				if (right_open) {
					right_text = "R open";
					right_textpos = cv::Point(eye_bb.x + 50, eye_bb.y - 25);
					cv::line(screen, cv::Point(eye_bb.x + eye_bb.width/2 + right_eye_rect.width/2 , eye_bb.y+ right_eye_rect.height/2), right_textpos, cvScalar(0, 0, 255));
				}

				cv::putText(screen, left_text, left_textpos, CV_FONT_HERSHEY_COMPLEX_SMALL,
					0.5, left_open ? cvScalar(0, 255, 0) : cvScalar(0, 0, 255), 1, CV_AA);
				cv::putText(screen, right_text, right_textpos, CV_FONT_HERSHEY_COMPLEX_SMALL,
					0.5, right_open ? cvScalar(0, 255, 0) : cvScalar(0, 0, 255), 1, CV_AA);

				// blit to screen
				cv::Mat bgr;
				cv::cvtColor(left_eye_part, bgr, CV_GRAY2BGR);
				bgr.copyTo(screen.rowRange(size.height, size.height*2).colRange(0, size.width));
				cv::cvtColor(right_eye_part, bgr, CV_GRAY2BGR);
				bgr.copyTo(screen.rowRange(size.height, size.height*2).colRange(size.width, size.width*2));
			}

			// Draw bounding rectangle for the eye
			cv::rectangle(screen, eye_bb, CV_RGB(0, 255, 0));
			cv::rectangle(screen, face_bb, CV_RGB(255, 255, 0));
		}

		if (eye_tpl.cols > 0 && eye_tpl.rows > 0) {
			cv::cvtColor(eye_tpl, eye_tpl_rgb, CV_GRAY2BGR);
			// http://stackoverflow.com/questions/10991523/opencv-draw-an-image-over-another-image
			eye_tpl_rgb.copyTo(screen.rowRange(0, eye_tpl.size().height).colRange(0, eye_tpl.size().width));
		}

		cv::putText(screen, "press q to quit", cv::Point2i(100, 10), CV_FONT_HERSHEY_COMPLEX_SMALL,
				0.5, cvScalar(192, 0, 192), 1, CV_AA);

		cv::rectangle(screen, cv::Rect(cv::Point(0, 0), left_eye_part.size()), CV_RGB(255, 0, 0));
		cv::rectangle(screen, cv::Rect(cv::Point(left_eye_part.size().width, 0), right_eye_part.size()), CV_RGB(255, 0, 255));
		// Display video
		cv::imshow("screen", screen);
		//cv::imshow("video", frame);
		//cv::imshow("gray", gray);
	}

	return 0;
}