#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>
#include <mmsystem.h>

#include <iostream>

void pressKey(int key, bool pressed) 
{
	INPUT inp;
	inp.type = INPUT_KEYBOARD;

	KEYBDINPUT& ki = inp.ki;
	ki.dwFlags = (pressed ? 0 : KEYEVENTF_KEYUP) | KEYEVENTF_SCANCODE;
	ki.dwExtraInfo = NULL;
	//ki.wVk = key;
	ki.wVk = 0;
	ki.wScan = key;
	ki.time = 0;

	int sent = SendInput(1, &inp, sizeof(inp));

	if (sent == 0)
		std::cerr << "couldn't send events" << std::endl;
}

void playWAV(const char* name)
{
	PlaySound(name, NULL, SND_FILENAME | SND_NODEFAULT | SND_NOWAIT | SND_ASYNC);
}
